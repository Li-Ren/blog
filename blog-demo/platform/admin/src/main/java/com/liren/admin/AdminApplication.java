package com.liren.admin;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @description: <h1>AdminApplication 启动类</h1>
 * @author: LiRen
 **/
@MapperScan("com.liren.admin.dao")
@SpringBootApplication()
public class AdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(AdminApplication.class, args);
    }
}
