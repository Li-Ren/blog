package com.liren.admin.dao;

import com.liren.admin.entity.User;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description: <h1>UserDao dao</h1>
 * @author: LiRen
 **/
@Repository
public interface UserDao {

    User selectByPrimaryKey(Integer id);

    List<User> selAllUser();

}
