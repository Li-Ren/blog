package com.liren.admin.service;

import com.liren.admin.entity.User;

import java.util.List;

/**
 * @description: MyUserService myuser接口层
 * @author: LiRen
 **/

public interface MyUserService {

    User queryuserbyid(String userId);

    List<User> getUserByPage();
}
