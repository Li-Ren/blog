# blog

#### 介绍
博客的代码或者互动记录，and so on.

#### 链接

<p align="center">
<a href="https://gitee.com/Li-Ren/blog" target="_blank">
	<img src="https://images.gitee.com/uploads/images/2020/0108/095111_d9d5e52a_1455503.png" width=""/>
</a>
</p>

<p align="center"><a href="#目录"><img src="https://img.shields.io/badge/阅读-read-green" alt="开始阅读"></a>   <a href="#微信"><img src="https://img.shields.io/badge/weChat-微信-green.svg" alt="投稿"></a>   <a href="https://blog.csdn.net/qq_26465035" target="_blank" ><img src="https://img.shields.io/badge/博客-CSDN-critical.svg" alt="投稿"></a>   <a href="https://blog.csdn.net/qq_26465035" target="_blank" ><img src="https://img.shields.io/badge/博客-简书-important.svg" alt="投稿"></a>   <a href="https://juejin.im/user/5ddc980ff265da7dd334fb24" target="_blank" ><img src="https://img.shields.io/badge/博客-掘金-blue.svg" alt="投稿"></a></p>

![](https://images.gitee.com/uploads/images/2019/1125/115830_a4e23b38_1455503.png)

### <a name="目录">目录</a>

- #### [【Java面试官】史上最全的JAVA专业术语面试100问](https://gitee.com/Li-Ren/blog/blob/master/Java-Interview-Onym/%E3%80%90Java%E9%9D%A2%E8%AF%95%E3%80%91%E5%8F%B2%E4%B8%8A%E6%9C%80%E5%85%A8%E7%9A%84JAVA%E4%B8%93%E4%B8%9A%E6%9C%AF%E8%AF%AD%E9%9D%A2%E8%AF%95100%E9%97%AE.md)

- #### [【Java面试官】史上最全的微服务专业术语面试50问](https://gitee.com/Li-Ren/blog/blob/master/Java-Interview-Onym/%E3%80%90Java%E9%9D%A2%E8%AF%95%E3%80%91%E5%8F%B2%E4%B8%8A%E6%9C%80%E5%85%A8%E7%9A%84%E5%BE%AE%E6%9C%8D%E5%8A%A1%E4%B8%93%E4%B8%9A%E6%9C%AF%E8%AF%AD%E9%9D%A2%E8%AF%95100%E9%97%AE.md )

- #### [【Java面试官】史上最全的JVM原理、调优和排查](https://gitee.com/Li-Ren/blog/blob/master/Java-Interview-Onym/%E3%80%90Java%E9%9D%A2%E8%AF%95%E5%AE%98%E3%80%91%E5%8F%B2%E4%B8%8A%E6%9C%80%E5%85%A8%E7%9A%84JVM%E5%8E%9F%E7%90%86%E3%80%81%E8%B0%83%E4%BC%98%E5%92%8C%E6%8E%92%E6%9F%A5.md)

